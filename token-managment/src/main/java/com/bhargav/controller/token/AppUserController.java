package com.bhargav.controller.token;


import com.bhargav.exceptions.ExceptionBuilder;
import com.bhargav.models.jwt.AuthenticateRequest;
import com.bhargav.models.jwt.AuthenticateResponse;
import com.bhargav.repository.security.JwtUserDetailService;
import com.bhargav.services.security.JwtUtility;
import com.bhargav.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import static com.bhargav.exceptions.ExceptionBuilder.buildError;
import static com.bhargav.utils.Utility.convertMillisecondToDateTime;
import static com.bhargav.utils.Utility.mapOf;

@RestController
@RequestMapping
public class AppUserController {
    private static final Logger logger = LoggerFactory.getLogger(AppUserController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtility jwtTokenUtil;
    @Autowired
    private JwtUserDetailService userDetailsService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticateRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassWord())
            );
        } catch (Exception e) {
            throw new InvalidDataAccessApiUsageException("Incorrect username or password", e);
        }
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUserName());
        final String jwt = jwtTokenUtil.generateToken(userDetails);
        String expiration = convertMillisecondToDateTime(jwtTokenUtil.extractExpiration(jwt).toInstant().toEpochMilli());
        return ResponseEntity.ok(new AuthenticateResponse(jwt,expiration));
    }

    @RequestMapping(value = "/validate", method = RequestMethod.GET)
    public ResponseEntity<?> validateToken(@RequestHeader("Authorization") String authorizationHeader) throws Exception {
        String expiration = "";
        try {
            String jwt = authorizationHeader.substring(7);
            String username = jwtTokenUtil.extractUsername(jwt);
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            jwtTokenUtil.validateToken(jwt, userDetails);
             expiration = convertMillisecondToDateTime(jwtTokenUtil.extractExpiration(jwt).toInstant().toEpochMilli());
        } catch (Exception e) {
            throw ExceptionBuilder.buildCrmException(122, "Authorization error", "UNKNOWN");
        }
        return ResponseEntity.ok(mapOf("status","valid","expiration",expiration));
    }
}
