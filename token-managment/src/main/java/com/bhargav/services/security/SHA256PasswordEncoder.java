package com.bhargav.services.security;

import org.springframework.security.crypto.password.PasswordEncoder;

import static com.bhargav.utils.Utility.convertToSha256Hex;

public final class SHA256PasswordEncoder implements PasswordEncoder {

    private static final PasswordEncoder INSTANCE = new SHA256PasswordEncoder();

    private SHA256PasswordEncoder() {
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return convertToSha256Hex(String.valueOf(rawPassword));
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encode(rawPassword).equals(encodedPassword);
    }

    public static PasswordEncoder getInstance() {
        return INSTANCE;
    }


}
