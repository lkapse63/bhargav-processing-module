package com.bhargav.contoller;

import com.bhargav.models.jwt.AuthenticateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RestController
@RequestMapping("/app")
public class TokenController {

    @Value("${bhargav.authenticate.path}")
    private String authenticatePath;
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticateRequest authenticationRequest, @RequestHeader Map<String,Object> requestHeader) throws Exception {
       return restTemplate.postForEntity(authenticatePath, authenticationRequest, Object.class, requestHeader);
    }
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public ResponseEntity<?> hello(){
        return ResponseEntity.ok("hello world..!");
    }

}
