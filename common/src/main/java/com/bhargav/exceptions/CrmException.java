package com.bhargav.exceptions;

import com.bhargav.models.CrmError;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CrmException extends RuntimeException{

    private CrmError crmError;

    public CrmException(CrmError crmError) {
        super(crmError.getErrorMessage());
        this.crmError = crmError;
    }

    public CrmException(String message, CrmError crmError) {
        super(message);
        this.crmError = crmError;
    }
}
