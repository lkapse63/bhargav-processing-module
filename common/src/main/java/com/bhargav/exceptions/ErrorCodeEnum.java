package com.bhargav.exceptions;

public enum ErrorCodeEnum {

    BAD_REQUEST_DATA{
        @Override
        public Integer getErrorCode() {
            return 100;
        }
    },
    DUPLICATE_REQUEST_DATA{
        @Override
        public Integer getErrorCode() {
            return 101;
        }
    },
    ;

    public abstract Integer getErrorCode();
}
