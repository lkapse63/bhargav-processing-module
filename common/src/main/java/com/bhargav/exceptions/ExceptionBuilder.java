package com.bhargav.exceptions;

import com.bhargav.models.CrmError;

import static com.bhargav.utils.Utility.convertMillisecondToDateTime;
import static com.bhargav.utils.Utility.getCurrentTimeInUTC;


public class ExceptionBuilder {

    public static CrmError buildError(Integer code, String message,String requestId){
       return CrmError.builder()
               .errorCode(code)
               .errorMessage(message)
               .status("fails")
               .timestamp(convertMillisecondToDateTime(getCurrentTimeInUTC()))
               .requestId(requestId)
               .build();
    }

    public static CrmException buildCrmException(Integer code, String message,String requestId){
        return new CrmException(buildError(code,message,requestId));
    }
}
