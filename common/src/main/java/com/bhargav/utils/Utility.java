package com.bhargav.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Utility implements Serializable {

    private static final long serialVersionUID = -2623908626314058510L;
    private static final Logger logger = LoggerFactory.getLogger(Utility.class);
    private static ObjectMapper mapper;
    private static final String MSG_EVT_DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final SimpleDateFormat sdf = new SimpleDateFormat(MSG_EVT_DATE_FORMAT);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MSG_EVT_DATE_FORMAT, Locale.getDefault());
    public static final String DB_WEEK_ARR[] = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static MessageDigest digest256;
    static {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            digest256 = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            logger.error("Unable to create message digest");
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String writeValueAsString(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public static Object readValueAsObject(String json, Class clazz) throws Exception {
        try {
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            return clazz.getDeclaredConstructor().newInstance();
        }
    }

    public static Object readValueAsObject(String json, TypeReference clazz) throws Exception {
        try {
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    /**
     * Get current time in UTC
     */
    public static long getCurrentTimeInUTC(){
        return Instant.ofEpochMilli(System.currentTimeMillis())
                .atZone(ZoneOffset.UTC)
                .toInstant().toEpochMilli();
    }

    public static String convertMillisecondToDateTime(Long timeInMillis) {
        LocalDateTime ldt = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeInMillis), ZoneId.systemDefault());
        return ldt.format(formatter);
    }

    public static Map<?,?> mapOf(Object... keyValue){
        if(keyValue.length % 2 !=0)
            new Exception("key and value not correct");
        Map<Object,Object> map = new HashMap<>();
        for(int i=1; i <= keyValue.length;i=i+2){
            map.put(keyValue[i-1],keyValue[i]);
        }
        return map;
    }

    public static List<?> listOf(Object... elements){
        return Arrays.stream(elements).collect(Collectors.toList());
    }

    public static String convertToSha256Hex(String input) {
        if(Objects.isNull(input) || input.equals("*") || input.equals("") || input.equals(" "))
            return null;

        byte[] encodedhash = digest256.digest(input.getBytes(StandardCharsets.UTF_8));
        StringBuilder hexString = new StringBuilder(2 * encodedhash.length);
        for (int i = 0; i < encodedhash.length; i++) {
            String hex = Integer.toHexString(0xff & encodedhash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
