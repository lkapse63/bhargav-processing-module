package com.bhargav.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    private Integer id;
    @JsonProperty(value = "username")
    private String userName;
    @JsonProperty(value = "password")
    private String passWord;
    @JsonProperty(value = "orgid")
    private Integer orgId;
    @JsonProperty(value = "usertype")
    private Integer userType;
    @JsonProperty(value = "firstname",required = true)
    private String firstName;
    @JsonProperty(value = "middlename")
    private String middleName;
    @JsonProperty(value = "lastname")
    private String lastName;
    @JsonProperty(value="phone1",required = true)
    private Long primaryContactNo;
    private Boolean primaryNoActive=Boolean.TRUE;;
    @JsonProperty(value = "phone2")
    private Long secondaryContactNo;
    private Boolean secondaryNoActive=Boolean.TRUE;
    @JsonProperty(value="email",required = true)
    private String email;
    @JsonProperty(value = "address")
    private List<Address> addressList;

}
