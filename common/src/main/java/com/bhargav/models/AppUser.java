package com.bhargav.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppUser {
    @JsonProperty(value = "username")
    private String userName;
    @JsonProperty(value="email")
    private String email;
    @JsonProperty(value="phone")
    private Long phoneNo;
    @JsonProperty(value = "password")
    private String passWord;
    @JsonProperty(value = "orgid")
    private Integer orgId;
    @JsonProperty(value = "usertype")
    private Integer userType;
    @JsonProperty(value = "crmuserid")
    private Integer crmUserId;
}
