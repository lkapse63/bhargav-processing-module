package com.bhargav.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CrmError {

    private String timestamp;
    private String status;
    private String path;
    private Integer errorCode;
    private String errorMessage;
    private String requestId;
    @JsonIgnore
    private String fullStackTrace;
}
