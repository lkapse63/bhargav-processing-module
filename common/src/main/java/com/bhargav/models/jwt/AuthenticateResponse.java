package com.bhargav.models.jwt;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;


@ToString
@Getter
public class AuthenticateResponse {
    @JsonProperty(value = "token")
    final private String token;
    @JsonProperty(value = "expiration")
    final private String expiration;

    public AuthenticateResponse(String token, String expiration) {
        this.token = "Bearer "+token;
        this.expiration = expiration;
    }
}
