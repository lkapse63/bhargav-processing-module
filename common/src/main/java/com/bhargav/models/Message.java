package com.bhargav.models;


import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@Getter
@Setter
public class Message implements Serializable {

    private static final long serialVersionUID = 5133960523134813473L;
    private static final Logger logger = LoggerFactory.getLogger(Message.class);

    private  Optional<Map<String,Object>> headers;
    private  Optional<Object> payload;
    private Optional<Object> additionalInfo;

    public Message(Optional<Map<String, Object>> headers, Optional<Object> payload) {
        this.headers = headers;
        this.payload = payload;
    }

    public Message(Optional<Map<String, Object>> headers, Optional<Object> payload, Optional<Object> additionalInfo) {
        this.headers = headers;
        this.payload = payload;
        this.additionalInfo = additionalInfo;
    }

    public Message() {
    }

    public static  Message buildMessage(Optional<Map<String,Object>> headers, Optional<Object> payload){
        return new Message(headers, payload);
    }


    public void setAdditionalInfo(Optional<Object> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
