package com.bhargav.services.ba;

import com.bhargav.models.CrmError;

public interface CRMErrorLogService {

    Integer saveError(CrmError crmError);
}
