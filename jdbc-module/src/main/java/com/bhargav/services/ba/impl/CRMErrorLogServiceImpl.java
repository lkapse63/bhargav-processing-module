package com.bhargav.services.ba.impl;

import com.bhargav.entity.common.CRMErrorLog;
import com.bhargav.models.CrmError;
import com.bhargav.repository.hrms.CRMErrorLogRepo;
import com.bhargav.services.ba.CRMErrorLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import static com.bhargav.utils.Utility.getCurrentTimeInUTC;

@Service("cRMErrorLogServiceImpl")
public class CRMErrorLogServiceImpl implements CRMErrorLogService {
    private static final Logger logger = LoggerFactory.getLogger(CRMErrorLogServiceImpl.class);
    @Autowired
    private CRMErrorLogRepo crmErrorLogRepo;
    @Override
    public Integer saveError(CrmError crmError) {
        Integer returnId=-1;
        CRMErrorLog crmErrorLog = CRMErrorLog.builder()
                .errorCode(crmError.getErrorCode())
                .errorMessage(crmError.getErrorMessage())
                .path(crmError.getPath())
                .requestId(crmError.getRequestId())
                .status(crmError.getStatus())
                .timestamp(crmError.getTimestamp())
                .fullStackTrace(crmError.getFullStackTrace())
                .build();
        crmErrorLog.setCreatedDate(getCurrentTimeInUTC());
        try{
            crmErrorLogRepo.save(crmErrorLog);
            returnId=crmErrorLog.getId();
        }catch (Exception ex){
            logger.error("Error while storing into Db {} error {}",crmError.getRequestId(),ex);
        }
        return returnId;
    }
}
