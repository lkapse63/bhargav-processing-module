package com.bhargav.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import static com.bhargav.utils.Utility.getCurrentTimeInUTC;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class BaseEntity {

    @Column(name = "created_date")
    private Long createdDate;
    @Column(name = "modify_date")
    private Long modifyDate = getCurrentTimeInUTC();
}
