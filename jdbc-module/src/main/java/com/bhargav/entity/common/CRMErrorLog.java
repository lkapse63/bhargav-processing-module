package com.bhargav.entity.common;

import com.bhargav.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "crm_error")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CRMErrorLog extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "timestamp")
    private String timestamp;
    @Column(name = "status")
    private String status;
    @Column(name = "path")
    private String path;
    @Column(name = "error_code")
    private Integer errorCode;
    @Column(name = "error_message")
    private String errorMessage;
    @Column(name = "request_id")
    private String requestId;
    @Column(name = "full_stack_trace", length = 500)
    private String fullStackTrace;
}
