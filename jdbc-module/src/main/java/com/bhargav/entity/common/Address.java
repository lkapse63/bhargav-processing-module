package com.bhargav.entity.common;

import com.bhargav.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class Address extends BaseEntity {

    @Column(name = "addressLine1")
    private String addrLine1;
    @Column(name = "addressLine2")
    private String addrLine2;
    @Column(name = "area")
    private String area;
    @Column(name = "city")
    private String city;
    @Column(name = "state")
    private String state;
    @Column(name = "country")
    private String country;
    @Column(name = "pin_code",nullable = false)
    private Integer pinCode;

}
