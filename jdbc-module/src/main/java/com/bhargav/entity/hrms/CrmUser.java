package com.bhargav.entity.hrms;

import com.bhargav.entity.BaseEntity;
import com.bhargav.entity.common.Address;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "crm_user")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CrmUser extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "primary_contact",nullable = false, unique = true)
    private Long primaryContactNo;
    @Column(name = "primary_active")
    private Boolean primaryNoActive=Boolean.TRUE;;
    @Column(name = "secondary_contact")
    private Long secondaryContactNo;
    @Column(name = "secondary_active")
    private Boolean secondaryNoActive=Boolean.TRUE;
    @Column(name = "email",nullable = false, unique = true)
    private String email;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "crm_user_addresses", joinColumns = @JoinColumn(name = "crm_user_id"))
    @AttributeOverrides({
            @AttributeOverride(name = "addrLine1", column = @Column(name = "office_address")),
            @AttributeOverride(name = "addrLine2", column = @Column(name = "home_address"))
    })
    private List<Address> addressList;
}
