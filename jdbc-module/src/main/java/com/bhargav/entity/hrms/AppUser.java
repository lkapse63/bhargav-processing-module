package com.bhargav.entity.hrms;

import com.bhargav.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "app_user")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AppUser extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private Integer id;
    @Column(name = "user_name",unique = true, nullable = false)
    private String userName;
    @Column(name = "email",unique = true, nullable = false)
    private String email;
    @Column(name = "phone",unique = true, nullable = false)
    private Long phoneNo;
    @Column(name = "password", nullable = false)
    private String passWord;
    @Column(name = "org_id")
    private Integer orgId;
    @Column(name = "user_type")
    private Integer userType;
    @Column(name = "crm_user_id")
    private Integer crmUserId;

}
