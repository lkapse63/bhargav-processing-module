package com.bhargav.entity.pcrs;

import com.bhargav.entity.BaseEntity;
import com.bhargav.entity.hrms.AppUser;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "echom_days")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Echom15Days extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="cust_id")
    private Customer customer;
    @Column(name="day_no")
    private Integer dayNo;
    @Column(name="serving_date")
    private Long servingDate;
    @Column(name="serviceType")
    private String serviceType;
    @Column(name="amount")
    private Double collectionAmount;
    @Column(name="incentive")
    private String incentive;
    @Column(name="remarks")
    private String remarks;

    @OneToOne
    @JoinColumn(name="app_user_id",referencedColumnName = "user_id")
    private AppUser appUser;
}
