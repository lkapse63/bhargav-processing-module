package com.bhargav.entity.pcrs;

import com.bhargav.entity.BaseEntity;
import com.bhargav.entity.common.Address;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customer")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cust_id", nullable = false)
    private Long id;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "primary_contact",nullable = false)
    private Integer primaryContactNo;
    @Column(name = "primary_active")
    private Boolean primaryNoActive;
    @Column(name = "secondary_contact")
    private Boolean secondaryContactNo=Boolean.TRUE;
    @Column(name = "secondary_active")
    private Boolean secondaryNoActive=Boolean.TRUE;
    @Column(name = "email")
    private String email;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "cust_addresses", joinColumns = @JoinColumn(name = "cust_id"))
    private List<Address> addressList;

}
