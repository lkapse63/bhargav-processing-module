package com.bhargav.entity.pcrs;

import com.bhargav.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "pcrs_product")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PcrsProduct extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "productId", nullable = false)
    private Integer productId;
    @Column(name = "date_purchase", nullable = false)
    private Long dateOfPurchase;
    @Column(name = "pin_code",nullable = false)
    private Integer pinCode;
    @Column(name = "area")
    private String area;
    @Column(name = "reviews",length = 500)
    private String reviews;
    @Column(name = "tc_name")
    private String tcName;
    @Column(name = "calling_code")
    private String callingCode;
}
