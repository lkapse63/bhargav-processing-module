package com.bhargav.entity.pcrs;

import com.bhargav.entity.BaseEntity;
import com.bhargav.entity.common.Address;
import com.bhargav.entity.hrms.Product;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "enquiry")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Enquiry extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private Product productId;
    @Column(name = "price_quote")
    private Double priceQuoted;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "enquiry_id", nullable = false)
    private String enquiryId;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "enquiry_addresses", joinColumns = @JoinColumn(name = "id"))
    @AttributeOverrides({
            @AttributeOverride(name = "addrLine1", column = @Column(name = "address")),
            @AttributeOverride(name = "addrLine2", column = @Column(name = "street"))
    })
    private List<Address> addressList;
}
