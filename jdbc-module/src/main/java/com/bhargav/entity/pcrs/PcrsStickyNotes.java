package com.bhargav.entity.pcrs;

import com.bhargav.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "pcrs_notes")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PcrsStickyNotes extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "notes",length = 500)
    private String notes;
    @Column(name = "user_id", nullable = false)
    private Integer userId;
}
