package com.bhargav.entity.pcrs;

import com.bhargav.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "echom")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Echom extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "remarks", nullable = true)
    private String remarks;
    @Column(name = "remark_date", nullable = true)
    private Long remarksDate;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="cust_id")
    private Customer customer;

}
