package com.bhargav.repository.hrms;

import com.bhargav.entity.hrms.AppUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends CrudRepository<AppUser,Integer> {

    @Query("From AppUser where userName=:userName and created_date=:createdAt")
    List<AppUser> getCreatedUser(@Param("userName") String userName, @Param("createdAt") Long createdAt);

    @Query("From AppUser where (userName=:userName or email=:email or phone=:phone ) and password=:password")
    List<AppUser> getUserForLoginCheck(@Param("userName") String userName, @Param("email") String email,@Param("phone") Long phone,@Param("password") String password);

    @Query("From AppUser where (userName=:userName or email=:email or phone=:phone )")
    AppUser getUserByUserName(@Param("userName") String userName, @Param("email") String email,@Param("phone") Integer phone);

}
