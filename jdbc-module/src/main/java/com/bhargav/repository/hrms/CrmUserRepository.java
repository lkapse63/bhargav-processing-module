package com.bhargav.repository.hrms;

import com.bhargav.entity.hrms.CrmUser;
import org.springframework.data.repository.CrudRepository;

public interface CrmUserRepository extends CrudRepository<CrmUser,Integer> {
}
