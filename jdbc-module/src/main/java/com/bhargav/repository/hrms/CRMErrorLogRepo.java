package com.bhargav.repository.hrms;

import com.bhargav.entity.common.CRMErrorLog;
import org.springframework.data.repository.CrudRepository;

public interface CRMErrorLogRepo extends CrudRepository<CRMErrorLog,Integer> {
}
