package com.bhargav.repository.security;

import com.bhargav.entity.hrms.AppUser;
import com.bhargav.repository.hrms.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailService implements UserDetailsService {
    @Autowired
    private UserRepository appUserRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Integer phoneNumber=0;
        try {
            phoneNumber= (Integer.valueOf(username));
        } catch (NumberFormatException ex) {
        }
        AppUser userByUserName = appUserRepository.getUserByUserName(username, username, phoneNumber);
        return new User(username, userByUserName.getPassWord(), new ArrayList<>());
    }
}
