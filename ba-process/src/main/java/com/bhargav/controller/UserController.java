package com.bhargav.controller;

import com.bhargav.entity.hrms.AppUser;
import com.bhargav.entity.hrms.CrmUser;
import com.bhargav.models.Address;
import com.bhargav.models.User;
import com.bhargav.repository.hrms.CrmUserRepository;
import com.bhargav.repository.hrms.UserRepository;
import com.bhargav.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/hrms")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private CrmUserRepository crmUserRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<?> createAppUser(@RequestBody User user) throws Exception {
        CrmUser crmUser = CrmUser.builder()
                .firstName(user.getFirstName())
                .middleName(user.getMiddleName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .primaryContactNo(user.getPrimaryContactNo())
                .secondaryContactNo(user.getSecondaryContactNo())
                .primaryNoActive(Boolean.TRUE)
                .secondaryNoActive(Boolean.TRUE)
                .build();
        List<Address> addressList = user.getAddressList();
        List<com.bhargav.entity.common.Address> addresses = addressList.stream().map(
                addr -> {
                    com.bhargav.entity.common.Address dbAddr = com.bhargav.entity.common.Address.builder()
                            .addrLine1(addr.getAddrLine1())
                            .addrLine2(addr.getAddrLine1())
                            .area(addr.getArea())
                            .city(addr.getCity())
                            .pinCode(addr.getPinCode())
                            .country(addr.getCountry())
                            .build();
                    dbAddr.setCreatedDate(Utility.getCurrentTimeInUTC());
                    return dbAddr;
                }
        ).collect(Collectors.toList());
        crmUser.setAddressList(addresses);
        crmUser.setCreatedDate(Utility.getCurrentTimeInUTC());
        try{
            CrmUser dbUser = crmUserRepository.save(crmUser);
            userRepository.save(AppUser.builder()
                            .crmUserId(dbUser.getId())
                            .email(dbUser.getEmail())
                            .phoneNo(dbUser.getPrimaryContactNo())
                            .userName(user.getUserName())
                            .passWord(Utility.convertToSha256Hex(user.getPassWord()))
                    .build());
            user.setId(dbUser.getId());
        }catch (DataAccessException ex){
            throw  ex;
        }
        return ResponseEntity.of(Optional.of(user));
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> createAppUser(@PathVariable("id") String userId) throws Exception{
        Optional<CrmUser> hrmsUser = crmUserRepository.findById(Integer.valueOf(userId));
        User user = new User();
        hrmsUser.ifPresent(crmUser -> {
            List<com.bhargav.entity.common.Address> addressList = crmUser.getAddressList();
            List<Address> addresses = addressList.stream()
                    .map(addr ->
                            Address.builder()
                                    .addrLine1(addr.getAddrLine1())
                                    .addrLine2(addr.getAddrLine2())
                                    .area(addr.getArea())
                                    .city(addr.getCity())
                                    .state(addr.getState())
                                    .country(addr.getCountry())
                                    .pinCode(addr.getPinCode())
                                    .build()
                    ).collect(Collectors.toList());
            user.setAddressList(addresses);
            user.setEmail(crmUser.getEmail());
            user.setFirstName(crmUser.getFirstName());
            user.setMiddleName(crmUser.getMiddleName());
            user.setLastName(crmUser.getLastName());
            user.setEmail(user.getEmail());
            user.setPrimaryContactNo(user.getPrimaryContactNo());
            user.setSecondaryContactNo(user.getSecondaryContactNo());
        });

        return ResponseEntity.ok(user);
    }


}
