package com.bhargav.handler;

import com.bhargav.exceptions.CrmException;
import com.bhargav.models.CrmError;
import com.bhargav.services.ba.CRMErrorLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import static com.bhargav.exceptions.ExceptionBuilder.buildCrmException;

@ControllerAdvice
public class ProcessGlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ProcessGlobalExceptionHandler.class);
    @Autowired
    private CRMErrorLogService crmErrorLogService;

    private static final String PIPE = "|";

    @ExceptionHandler(CrmException.class)
    public ResponseEntity<?> handleCrmExceptionException(CrmException exception, WebRequest wbr){
        return getCrmErrorResponseEntity(exception, wbr, null);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleNullPointerException(NullPointerException exception, WebRequest wbr){
        Optional<String> s = Arrays.stream(exception.getStackTrace()).findFirst()
                .map(elem ->elem.toString());
        String stackTrace = s.orElse(null);
        CrmException crmException = buildCrmException(120,NullPointerException.class.getSimpleName(), "UNKOWN");
        return getCrmErrorResponseEntity(crmException, wbr, stackTrace);
    }

    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<?> handleDataAccessException(DataAccessException exception, WebRequest wbr){
        Optional<String> s = Arrays.stream(exception.getStackTrace())
                .filter(st -> st.getClassName().contains("com.bhargav"))
                .findFirst()
                .map(elem ->elem.toString());
        String stackTrace = s.orElse(null);
        Optional<Throwable> rootCause = Stream.iterate(exception, Throwable::getCause)
                .filter(element -> element.getCause() == null)
                .findFirst();
        CrmException crmException = buildCrmException(121, rootCause.get().getMessage(), "UNKOWN");
        return getCrmErrorResponseEntity(crmException, wbr, stackTrace);
    }

    /**
     * Global exception handler
     * @param exception
     * @param wbr
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleGlobalException(Exception exception, WebRequest wbr){
        Optional<String> s = Arrays.stream(exception.getStackTrace()).findFirst()
                .map(elem ->elem.toString());
        String stackTrace = s.orElse(null);
        CrmException crmException = buildCrmException(130, HttpStatus.INTERNAL_SERVER_ERROR.toString(), "UNKOWN");
        return getCrmErrorResponseEntity(crmException, wbr, stackTrace);
    }

    private ResponseEntity<CrmError> getCrmErrorResponseEntity(CrmException exception, WebRequest wbr, String stackTrace) {
        CrmError crmError = exception.getCrmError();
        crmError.setFullStackTrace(stackTrace);
        crmError.setPath(wbr.getDescription(true));
        logger.error("Error has been occur for {} error response {}", wbr.getDescription(false),crmError);
        crmErrorLogService.saveError(crmError);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(crmError);
    }
}
